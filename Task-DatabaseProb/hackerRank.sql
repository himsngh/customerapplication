-- Print Prime Numbers **

-- Occupations (Pivot) ***

-- 15 Days of Learning SQL ***

-- ***************************************************************************************

-- contest leaderboard

select h.hacker_id, name, sum(score) as total_score from hackers h
inner join (select hacker_id, max(score) as score from submissions group by hacker_id, challenge_id) max_score
on h.hacker_id = max_score.hacker_id
group by h.hacker_id, name
having total_score > 0
order by total_score desc, h.hacker_id;

        -- Fail ho jayega when we have multiple high scores of same value 
        
            -- select s.hacker_id, h.name , sum(s.score) as total_score from submissions s
            -- inner join hackers h on s.hacker_id = h.hacker_id
            -- where score = (select max(score) from submissions s1
            --                 where s.hacker_id = s1.hacker_id and s.challenge_id = s1.challenge_id)
            -- group by s.hacker_id, h.name
            -- having total_score > 0
            -- order by total_score desc, s.hacker_id;


-- Weather observation 20 

		-- Oracle 

			select round(median(lat_n), 4) from station

select round(s.lat_n,4) from station s
where (select count(lat_n) from station s1 where s.lat_n > s1.lat_n) = 
(select count(lat_n) from station s2 where s.lat_n < s2.lat_n);


-- SQL Project Planning ***

SELECT Start_Date, MIN(End_Date)
FROM 
(SELECT Start_Date FROM Projects WHERE Start_Date NOT IN (SELECT End_Date FROM Projects)) a,
(SELECT end_date FROM PROJECTS WHERE end_date NOT IN (SELECT start_date FROM PROJECTS)) b

where start_date < end_date
GROUP BY start_date
ORDER BY datediff(start_date, MIN(end_date)) DESC, start_date


-- Interviews **
select c.contest_id,c.hacker_id, c.name, 
sum(total_submissions), sum(total_accepted_submissions), sum(total_views), sum(total_unique_views)
from contests c 
join colleges col on c.contest_id = col.contest_id 
join challenges cha on  col.college_id = cha.college_id 
left join
(select challenge_id, sum(total_views) as total_views, sum(total_unique_views) as total_unique_views
from view_stats group by challenge_id) vs on cha.challenge_id = vs.challenge_id 
left join
(select challenge_id, sum(total_submissions) as total_submissions, sum(total_accepted_submissions) as total_accepted_submissions from submission_stats group by challenge_id) ss on cha.challenge_id = ss.challenge_id
group by c.contest_id, c.hacker_id, c.name
having sum(total_submissions)!=0 or 
        sum(total_accepted_submissions)!=0 or
        sum(total_views)!=0 or
        sum(total_unique_views)!=0
order by c.contest_id;


-- Ollivanders Inventory 

select w.id, wp.age, w.coins_needed, w.power 
from wands w 
inner join wands_property wp on w.code = wp.code
where wp.is_evil = 0 and w.coins_needed = 
(select min(coins_needed) from wands w1 
inner join wands_property wp1 on w1.code = wp1.code 
where w1.power = w.power and wp1.age = wp.age)
order by w.power desc, wp.age desc; 

-- Symetry Pair 

SELECT f1.X, f1.Y FROM Functions f1
INNER JOIN Functions f2 ON f1.X=f2.Y AND f1.Y=f2.X
GROUP BY f1.X, f1.Y
HAVING COUNT(f1.X)>1 or f1.X<f1.Y
ORDER BY f1.X; 


-- Challenges 

select c.hacker_id, h.name, count(c.hacker_id) from challenges c
inner join hackers h on c.hacker_id = h.hacker_id
group by c.hacker_id, h.name
having 
    count(c.hacker_id) = 
    (select max(temp1.c_count) from (select count(c3.hacker_id) as c_count from challenges c3 group by c3.hacker_id) temp1)
    
    or 
    c.hacker_id in
    
    (select c1.hacker_id from challenges c1
     group by c1.hacker_id
     having count(c1.hacker_id) not in 
     (select count(c2.hacker_id) from challenges c2 where c1.hacker_id != c2.hacker_id 
      group by c2.hacker_id)
    )
order by count(c.hacker_id) desc, c.hacker_id;


-- Placements 

select s.name from students s
inner join friends f  on s.id = f.id
inner join packages p on f.friend_id = p.id
where p.salary > (select salary from packages p1 where p1.id = s.id)
order by p.salary;

-- Weather Observation Station 18

select 
round(abs(max(lat_n) - min(lat_n)) + abs(max(long_w) - min(long_w)),4) from Station;

-- Weather Observation Station 19

select 
round(power(power(max(lat_n) - min(lat_n),2) + power(max(long_w) - min(long_w),2),0.5),4) from Station;

-- New Companies 

select c.company_code, c.founder, 
count(distinct l.lead_manager_code), count(distinct s.senior_manager_code),count(distinct m.manager_code), count(distinct e.employee_code) 
from company c
inner join lead_manager l on l.company_code = c.company_code
inner join senior_manager s on s.company_code = c.company_code
inner join manager m on m.company_code = c.company_code
inner join employee e on e.company_code = c.company_code
group by c.company_code, c.founder
order by c.company_code;


-- THE PADS 

select concat(name,
case 
    when occupation = 'Doctor' then '(D)'
    when occupation = 'Actor' then '(A)'
    when occupation = 'Professor' then '(P)'
    when occupation = 'Singer' then '(S)'
end) 
from occupations
order by name;

select concat('There are a total of ',count(occupation),' ',
Lower(occupation), 
case 
    when count(occupation) > 1 then 's.' 
    else '.' 
end)
from occupations 
group by occupation
order by count(occupation);

-- Binary Tree Nodes

select n,
case 
    when p is null then 'Root'
    when n in (select p from bst) then 'Inner'
    else 'Leaf'
end
from bst
order by n;

-- The Report

select if(g.grade >= 8 , s.name, 'NULL'), g.grade , s.marks from  students s, grades g
where s.marks between g.min_mark and g.max_mark 
order by g.grade desc,s.name asc,s.marks asc;


-- Top Competitors 

select h.hacker_id, h.name from submissions s
inner join challenges c on s.challenge_id = c.challenge_id
inner join difficulty d on c.difficulty_level = d.difficulty_level
inner join hackers h on s.hacker_id = h.hacker_id
where d.score = s.score 
group by s.hacker_id, h.name
having count(s.hacker_id) > 1
order by count(s.hacker_id) desc, s.hacker_id;
