package controller

import (
	"context"
	"errors"
	"gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/server/database"
	"log"
	"sync"

	pb "gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/generate/customerProto"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CustomerServiceController struct {
	pb.UnimplementedCustomersServer
	mu   sync.Mutex
	db *database.Database
}


func NewCustomerServiceController() *CustomerServiceController {

	return &CustomerServiceController{
		db: database.NewDatabase(),
	}
}

func (s *CustomerServiceController) CreateCustomer(ctx context.Context, r *pb.CreateCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetCustomer() == nil {
		return nil, errors.New("no customer provided")
	}

	err := s.db.CreateCustomer(r.Customer)
	if err != nil {
		return nil, err
	}

	log.Println("new customer created with id : ", r.Customer.Id)
	return r.Customer, nil
}

func (s *CustomerServiceController) GetCustomer(ctx context.Context, r *pb.GetCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	customer, err := s.db.GetCustomer(r.Id)
	if err != nil {
		return nil, err
	}

	return customer, nil

}

func (s *CustomerServiceController) ListCustomers(context.Context, *emptypb.Empty) (*pb.ListCustomersResponse, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	customers, err := s.db.ListCustomer()
	if err != nil {
		return nil, err
	}

	return &pb.ListCustomersResponse{Customers: customers}, nil
}

func (s *CustomerServiceController) UpdateCustomer(ctx context.Context, r *pb.UpdateCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetCustomer() == nil {
		return nil, errors.New("no customer provided")
	}

	err := s.db.UpdateCustomer(r.Customer)
	if err != nil {
		return nil, err
	}

	log.Println("customer update with id : ", r.Customer.Id)
	return r.Customer, nil
}

func (s *CustomerServiceController) DeleteCustomer(ctx context.Context, r *pb.DeleteCustomerRequest) (*emptypb.Empty, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	err := s.db.DeleteCustomer(r.Id)
	if err != nil {
		return nil, err
	}

	log.Println("customer deleted from the database with id : ", r.Id)
	return &emptypb.Empty{}, nil
}

func (s *CustomerServiceController) SearchCustomer(ctx context.Context, r *pb.SearchCustomerRequest) (*pb.SearchCustomerResponse, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetFirstName() == "" && r.GetLastName() == "" && r.GetEmail() == "" && r.GetPhoneNo() == 0 {
		return nil, errors.New("no search parameter provided")
	}

	customers, err := s.db.SearchCustomer(r)
	if err != nil {
		return nil, err
	}

	return &pb.SearchCustomerResponse{Customers: customers}, nil
}
