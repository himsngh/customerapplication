package database

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	pb "gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/generate/customerProto"
)

type Database struct {
	db *sql.DB
}

func NewDatabase() *Database {
	return &Database{
		db:Connect(),
	}
}

func Connect() *sql.DB{
	// to connect to the docker database
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"customer-database", 5432, "himanshuappointy", "password", "himanshuappointy")

	// to connect to local database
	//psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	//	"localhost", 5432, "himanshuappointy", "password", "himanshuappointy")

	psqlDb, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalln("database connection failed ", err)
	}

	err = psqlDb.Ping()
	if err != nil {
		log.Println("pint database failed")
		panic(err)
	}

	log.Println("database connected..........................")

	createTableIfNotExist(psqlDb)

	return psqlDb
}

func createTableIfNotExist(psql *sql.DB) {
	_, err := psql.Exec("create table if not exists customers (" +
		"id int primary key ," +
		"firstname varchar(100), " +
		"lastname varchar(100), " +
		"email varchar(100) not null, " +
		"phone_no bigint )")

	if err != nil {
		log.Println("error creating table.")
	}
}


func (d *Database) CreateCustomer(newCustomer *pb.Customer) error {

	_, err := d.db.Exec("INSERT INTO CUSTOMERS VALUES ($1, $2, $3, $4, $5)",
		newCustomer.Id, newCustomer.FirstName, newCustomer.LastName, newCustomer.Email, newCustomer.PhoneNo)

	if err != nil {
		log.Println("Failed to insert ")
		return errors.New("Failed to insert the user : " + err.Error())
	}

	return nil
}

func (d *Database) GetCustomer(id int32) (*pb.Customer , error) {

	row := d.db.QueryRow("select * from customers where id = $1", id)

	customer := &pb.Customer{}

	err := row.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Email, &customer.PhoneNo)
	if err != nil {
		return nil, errors.New("No such customer exist : " + err.Error())
	}

	return customer, nil
}


func (d *Database) ListCustomer() ([]*pb.Customer, error) {

	rows, err := d.db.Query("select * from customers")
	if err != nil {
		return nil, errors.New("No customer exist : " + err.Error())
	}

	customers := make([]*pb.Customer, 0)

	for rows.Next() {
		customer := &pb.Customer{}

		err := rows.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Email, &customer.PhoneNo)
		if err != nil {
			log.Println(err.Error())
			continue
		}

		customers = append(customers, customer)
	}

	return customers, nil
}

func (d *Database) UpdateCustomer(customer *pb.Customer) error {

	_, err := d.db.Exec("update customers set firstname=$2, lastname=$3, email=$4, phoneno=$5 where id=$1",
		customer.Id, customer.FirstName, customer.LastName, customer.Email, customer.PhoneNo)
	if err != nil {
		return errors.New("error updating the row : " + err.Error())
	}

	return nil
}

func (d *Database) DeleteCustomer(id int32) error {

	res, err := d.db.Exec("delete from customers where id = $1", id)
	if err != nil {
		return errors.New("error deleting the customer : " + err.Error())
	}

	if n, _ := res.RowsAffected(); n == 0 {
		return errors.New("no such customer exist")
	}
	return nil
}

func (d *Database) SearchCustomer(r *pb.SearchCustomerRequest) ([]*pb.Customer, error){

	customers := make([]*pb.Customer, 0)

	// If the user has provided the email in the query then there may exist only one user as the email is unique.
	if r.GetEmail() != "" {
		newCustomer := &pb.Customer{}

		row := d.db.QueryRow("select * from customers where email=$1", r.Email)

		err := row.Scan(&newCustomer.Id, &newCustomer.FirstName, &newCustomer.LastName, &newCustomer.Email, &newCustomer.PhoneNo)
		if err != nil {
			return nil, errors.New("No such customer exist : " + err.Error())
		}

		if r.GetFirstName() != "" && r.GetFirstName() != newCustomer.FirstName {
			return nil, customerDoesntExistError()
		}

		if r.GetLastName() != "" && r.GetLastName() != newCustomer.LastName {
			return nil, customerDoesntExistError()
		}

		if r.GetPhoneNo() != 0 && r.GetPhoneNo() != newCustomer.PhoneNo {
			return nil, customerDoesntExistError()
		}

		customers = append(customers, newCustomer)
		return customers, nil
	}

	//// Searching the customer in the database with customer details as firstName, lastName and phoneNo.

	if r.GetFirstName() != "" && r.GetLastName() != "" && r.GetPhoneNo() != 0 {

		rows, err := d.db.Query("select * from customers where firstname=$1 and lastname=$2 and phoneno=$3", r.FirstName, r.LastName, r.PhoneNo)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetFirstName() != "" && r.GetLastName() != "" {

		rows, err := d.db.Query("select * from customers where firstname=$1 and lastname=$2", r.FirstName, r.LastName)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetFirstName() != "" && r.GetPhoneNo() != 0 {

		rows, err := d.db.Query("select * from customers where firstname=$1 and phoneno=$2", r.FirstName, r.PhoneNo)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetLastName() != "" && r.GetPhoneNo() != 0 {
		rows, err := d.db.Query("select * from customers where lastname=$1 and phoneno=$2", r.LastName, r.PhoneNo)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetFirstName() != "" {
		rows, err := d.db.Query("select * from customers where firstname=$1", r.FirstName)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetLastName() != "" {
		rows, err := d.db.Query("select * from customers where lastname=$1", r.LastName)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	if r.GetPhoneNo() != 0 {
		rows, err := d.db.Query("select * from customers where phoneno=$1", r.PhoneNo)
		if err != nil {
			return nil, customerDoesntExistError()
		}

		customers = queryCustomers(rows)
		return customers, nil
	}

	return customers, nil
}

func queryCustomers(rows *sql.Rows) []*pb.Customer {

	customers := make([]*pb.Customer, 0)

	for rows.Next() {
		customer := &pb.Customer{}

		err := rows.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Email, &customer.PhoneNo)
		if err != nil {
			log.Println(err.Error())
			continue
		}

		customers = append(customers, customer)
	}
	return customers

}

func customerDoesntExistError() error {
	return errors.New("no such customer exist")
}
