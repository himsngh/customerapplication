package main

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	pb "gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/generate/customerProto"
	"gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/server/controller"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"strings"
)

func main() {

	cs := controller.NewCustomerServiceController()

	grpcServer := grpc.NewServer()
	pb.RegisterCustomersServer(grpcServer,cs)

	httpServer := runtime.NewServeMux()
	err := pb.RegisterCustomersHandlerServer(context.Background(), httpServer, cs)
	if err != nil {
		log.Fatal("error registering http server : ", err)
	}

	log.Fatal(http.ListenAndServe(":8080", grpcServerHandler(grpcServer, httpServer)))
}

func grpcServerHandler(grpcS *grpc.Server, httpS http.Handler) http.Handler {

	return h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcS.ServeHTTP(w, r)
		} else {
			httpS.ServeHTTP(w, r)
		}
	}), &http2.Server{})
}
