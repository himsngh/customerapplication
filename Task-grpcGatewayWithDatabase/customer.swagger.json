{
  "swagger": "2.0",
  "info": {
    "title": "customer.proto",
    "version": "version not set"
  },
  "tags": [
    {
      "name": "Customers"
    }
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/api/v1/customer": {
      "post": {
        "summary": "Call to create a new customer in the database with input details.",
        "operationId": "Customers_CreateCustomer",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/Customer"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CreateCustomerRequest"
            }
          }
        ],
        "tags": [
          "Customers"
        ]
      }
    },
    "/api/v1/customer/{id}": {
      "get": {
        "summary": "Call to get a customer from the database using the unique id.",
        "operationId": "Customers_GetCustomer",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/Customer"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          }
        ],
        "tags": [
          "Customers"
        ]
      }
    },
    "/api/v1/customers": {
      "get": {
        "summary": "Call to get the list of all the customers in the database.",
        "operationId": "Customers_ListCustomers",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/ListCustomersResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "tags": [
          "Customers"
        ]
      }
    },
    "/api/v1/customers/delete/{id}": {
      "delete": {
        "summary": "Call to delete a customer corresponding to its unique id.",
        "operationId": "Customers_DeleteCustomer",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "properties": {}
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          }
        ],
        "tags": [
          "Customers"
        ]
      }
    },
    "/api/v1/customers/search": {
      "get": {
        "summary": "Call to search a customer by details.",
        "operationId": "Customers_SearchCustomer",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/SearchCustomerResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "firstName",
            "description": "All these fields are passed as url query parameters.",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "lastName",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "email",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "phoneNo",
            "in": "query",
            "required": false,
            "type": "string",
            "format": "int64"
          }
        ],
        "tags": [
          "Customers"
        ]
      }
    },
    "/api/v1/customers/update": {
      "put": {
        "summary": "Call to update details of customers corresponding to its unique id.",
        "operationId": "Customers_UpdateCustomer",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/Customer"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/UpdateCustomerRequest"
            }
          }
        ],
        "tags": [
          "Customers"
        ]
      }
    }
  },
  "definitions": {
    "CreateCustomerRequest": {
      "type": "object",
      "properties": {
        "customer": {
          "$ref": "#/definitions/Customer"
        }
      }
    },
    "Customer": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32",
          "title": "unique identifier of customer"
        },
        "firstName": {
          "type": "string",
          "title": "Customer personal details"
        },
        "lastName": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "phoneNo": {
          "type": "string",
          "format": "int64"
        }
      }
    },
    "ListCustomersResponse": {
      "type": "object",
      "properties": {
        "customers": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Customer"
          }
        }
      }
    },
    "SearchCustomerResponse": {
      "type": "object",
      "properties": {
        "customers": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Customer"
          }
        }
      }
    },
    "UpdateCustomerRequest": {
      "type": "object",
      "properties": {
        "customer": {
          "$ref": "#/definitions/Customer"
        }
      }
    },
    "protobufAny": {
      "type": "object",
      "properties": {
        "typeUrl": {
          "type": "string"
        },
        "value": {
          "type": "string",
          "format": "byte"
        }
      }
    },
    "rpcStatus": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/protobufAny"
          }
        }
      }
    }
  }
}
