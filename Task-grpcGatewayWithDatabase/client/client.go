package main

import (
	"context"
	"fmt"
	pb "gitlab.com/himsngh/CustomerApplication/Task-grpcGatewayWithDatabase/generate/customerProto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

func main() {

	conn, err := grpc.DialContext(context.Background(), "localhost:8080", grpc.WithInsecure())

	if err != nil {
		fmt.Println("Failed to dial the server: ", err)
		return
	}
	defer conn.Close()

	client := pb.NewCustomersClient(conn)

	createCustomer(client)
	getCustomer(client)
	listCustomer(client)
	updateCustomer(client)
	searchCustomer(client)
	deleteCustomer(client)

	// ------------------------ Http Client Side functions ---------------------------- //
	fmt.Println("\n------------- REST CLIENT SIDE -------------------")

	createUser()
	listAllUsers()
	deleteUser()
	updateUser()
	searchUser()
}

func createCustomer(client pb.CustomersClient) {

	newCustomer :=  &pb.CreateCustomerRequest{Customer: &pb.Customer{
		Id:        123,
		FirstName: "Hello",
		LastName:  "Go",
		Email:     "helloGo@appointy.com",
		PhoneNo:   648828292,
	}}

	customer, err := client.CreateCustomer(context.Background(),newCustomer)
	if err != nil {
		fmt.Println("Error creating customer", err)
	}

	fmt.Println(customer)

	newCustomer =  &pb.CreateCustomerRequest{Customer: &pb.Customer{
		Id:        8129,
		FirstName: "Hello",
		LastName:  "World",
		Email:     "hellow@gmal.com",
		PhoneNo:   648828292,
	}}

	customer, err = client.CreateCustomer(context.Background(),newCustomer)
	if err != nil {
		fmt.Println("Error creating customer", err)
		return
	}

	fmt.Println(customer)

	newCustomer =  &pb.CreateCustomerRequest{Customer: &pb.Customer{
		Id:        229,
		FirstName: "Hello",
		LastName:  "Gopher",
		Email:     "golang@gmal.com",
		PhoneNo:   648828292,
	}}

	customer, err = client.CreateCustomer(context.Background(),newCustomer)
	if err != nil {
		fmt.Println("Error creating customer", err)
		return
	}

	fmt.Println(customer)

	newCustomer =  &pb.CreateCustomerRequest{Customer: &pb.Customer{
		Id:        12,
		FirstName: "newUser",
		LastName:  "go",
		Email:     "new@gmal.com",
		PhoneNo:   648828292,
	}}

	customer, err = client.CreateCustomer(context.Background(),newCustomer)
	if err != nil {
		fmt.Println("Error creating customer", err)
		return
	}

	fmt.Println(customer)
}

func getCustomer(client pb.CustomersClient) {

	customer, err := client.GetCustomer(context.Background(), &pb.GetCustomerRequest{Id: 8129})
	if err != nil {
		fmt.Println("Error getting customer", err)
		return
	}

	fmt.Println(customer)
}

func listCustomer(client pb.CustomersClient) {

	customers, err := client.ListCustomers(context.Background(), &emptypb.Empty{})
	if err != nil {
		fmt.Println("Error listing customer", err)
		return
	}

	fmt.Println(customers)
}

func updateCustomer(client pb.CustomersClient) {
	newCustomer :=  &pb.UpdateCustomerRequest{Customer: &pb.Customer{
		Id:        12,
		FirstName: "Himanshu",
		LastName:  "Ranjan",
		Email:     "himanshuranjan@appointy.com",
		PhoneNo:   648828292,
	}}

	customers, err := client.UpdateCustomer(context.Background(), newCustomer)
	if err != nil {
		fmt.Println("Error updating customer", err)
		return
	}

	fmt.Println(customers)
}

func deleteCustomer(client pb.CustomersClient) {
	_, err := client.DeleteCustomer(context.Background(), &pb.DeleteCustomerRequest{Id: 12})
	if err != nil {
		fmt.Println("Error deleting customer", err)
		return
	}

}

func searchCustomer(client pb.CustomersClient) {
	customers, err := client.SearchCustomer(context.Background(), &pb.SearchCustomerRequest{
		FirstName: "Hello",
		PhoneNo: 648828292,
	})
	if err != nil {
		fmt.Println("Error searching customer", err)
		return
	}

	fmt.Println("search result : " , customers)
}
