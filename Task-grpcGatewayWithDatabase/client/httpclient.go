package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func createUser(){

	res, err := http.Post("http://localhost:8080/api/v1/customer",
		"application/json; charset=UTF-8",
		strings.NewReader(`
						{ "customer" : 
								{
									"id"     	: 231,
									"firstName" : "Himanshu",
									"lastName"  : "Ranjan",
									"email"     : "hr@appointy.com",
									"phoneNo"	: 6382106201
								}
						}
		`))

	if err != nil{
		log.Println("Error Creating User : ",err)
		return
	}
	defer res.Body.Close()

	r, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("Error creating user")
		return
	}

	fmt.Println("Create User :", string(r))
}

func listAllUsers(){

	res, err := http.Get("http://localhost:8080/api/v1/customers")

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("List All User : ", res.Status)
	fmt.Println(string(data))

}

func deleteUser(){

	req, err := http.NewRequest(http.MethodDelete,"http://localhost:8080/api/v1/customers/delete/123", nil)
	res , err := http.DefaultClient.Do(req)

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	fmt.Println("Delete User : ", res.Status)
}

func updateUser(){
	// Use method patch when updating some attributes and method put when updating all the attributes
	req, err := http.NewRequest(http.MethodPut, "http://localhost:8080/api/v1/customers/update",
		strings.NewReader(`
							{ "customer" : 
								{
									"id"     	: 231,
									"firstName" : "Himanshu",
									"lastName"  : "Ranjan",
									"email"     : "himansh3@appointy.com",
									"phoneNo"	: 6382106201
								}
						}
		`))

	res , err := http.DefaultClient.Do(req)
	if err != nil{
		log.Println("Error Creating User : ",err)
		return
	}
	defer res.Body.Close()
	data , _ := ioutil.ReadAll(res.Body)

	fmt.Println("Update User : ", res.Status)
	fmt.Println(string(data))
}

func searchUser(){

	res, err := http.Get("http://localhost:8080/api/v1/customers/search?firstName=Hello")

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("Search User :\t", res.Status)
	fmt.Println("Search Result :\t", string(data))
}

