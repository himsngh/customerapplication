package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func main(){

	// Create Users using the body of the request
	createUserByBody()

	// Create user using the url query string
	createUser()

	// List All Users
	listAllUsers()

	// Delete User using query delete?id=x;
	deleteUser()

	// Update User
	updateUser()

	// SearchUser
	searchUser()

}

func createUser(){
	res, err := http.Post("http://localhost:8080/api/v1/user?id=1234&firstName=Himanshu&lastName=Ranjan&email=himansh.singh3@gmail.com&phoneNo=5435345",
						"application/json;charset=UTF-8;",nil)

	if err != nil {
		fmt.Println("Error Creating User : ", err.Error())
		return
	}
	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("Create User : ", res.Status)
	fmt.Println(string(data))
}


func createUserByBody(){

	res, err := http.Post("http://localhost:8080/api/v1/create",
		"application/json; charset=UTF-8",
		strings.NewReader(`
						 	{
								"id"     	: 1,
								"firstName" : "Himanshu",
								"lastName"  : "Ranjan",
								"email"     : "himanshuranjan@appointy.com",
								"phoneNo"	: 6382106201
							}
		`))

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	fmt.Println("Create User (Body) :", res.Status)
}

func listAllUsers(){

	res, err := http.Get("http://localhost:8080/api/v1/users")

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("List All User : ", res.Status)
	fmt.Println(string(data))

}

func deleteUser(){

	req, err := http.NewRequest(http.MethodDelete,"http://localhost:8080/api/v1/users/delete?id=1234", nil)
	res , err := http.DefaultClient.Do(req)

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("Delete User : ", res.Status)
	fmt.Println(string(data))
}

func updateUser(){
	// Use method patch when updating some attributes and method put when updating all the attributes
	req, err := http.NewRequest(http.MethodPatch, "http://localhost:8080/api/v1/users/update?id=1&firstName=UpdatedFName&lastName=UpdatedLName", nil)
	res , err := http.DefaultClient.Do(req)

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("Update User : ", res.Status)
	fmt.Println(string(data))
}

func searchUser(){

	res, err := http.Get("http://localhost:8080/api/v1/users/search?id=1")

	if err != nil{
		log.Fatal("Error Creating User : ",err)
	}

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println("Search User :\t", res.Status)
	fmt.Println("Search Result :\t", string(data))
}