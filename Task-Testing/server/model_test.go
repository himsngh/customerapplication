package main

import (
	"reflect"
	"testing"
)

func fillCustomer() CustomerList{

	newCustomerList := CustomerList{
		1 : NewCustomer(1,"Hello1","World","hello@world.com",5323),
		2 : NewCustomer(2,"Hello2","World","world@hello.com",5323),
	}

	return newCustomerList
}


func TestCustomerList_Add(t *testing.T) {
	type args struct {
		customer *Customer
	}

	newCustomerList := fillCustomer()

	var tests = []struct {
		name    string
		c       CustomerList
		args    args
		wantErr bool
	}{
		{
			name: "Testing-Add-1",
			args: args{customer: NewCustomer(3,"himanshu","ranjan","himanshuranjan@appointy.com",5323)},
			c: newCustomerList,
			wantErr: false,
		},
		{
			name: "Testing-Add-2",
			args: args{customer: NewCustomer(1,"Hello","World","hello@world.com",5323)},
			c : newCustomerList,
			wantErr: true,
		},
		{
			name: "Testing-Add-3",
			args: args{customer: NewCustomer(4,"Hello","World","hello@world.com",5323)},
			c: newCustomerList,
			wantErr: true,
		},
		{
			name : "Testing-Add-4",
			args : args{customer: NewCustomer(0, "Hello", "World", "fdad", 35235)},
			c : newCustomerList,
			wantErr: true,
		},
		{
			name : "Testing-5-add",
			args : args{customer: NewCustomer(124, "Hello", "Go", "", 452634)},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.c.Add(tt.args.customer); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCustomerList_Delete(t *testing.T) {
	type args struct {
		id int
	}

	newCustomerList := fillCustomer()

	tests := []struct {
		name    string
		c       CustomerList
		args    args
		wantErr bool
	}{
		{
			name: "Testing-delete-1",
			args: args{id: 1},
			c: newCustomerList,
			wantErr: false,
		},
		{
			name: "Testing-delete-2",
			args: args{id: 1},
			c: newCustomerList,
			wantErr: true,
		},
		{
			name: "Testing-delete-3",
			args: args{id: 124142},
			c: newCustomerList,
			wantErr: true,
		},

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.c.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCustomerList_List(t *testing.T) {
	newCustomerList := fillCustomer()
	delete(newCustomerList, 2)

	tests := []struct {
		name string
		c    CustomerList
		want []*Customer
	}{
		{
			name: "Testing-List-1",
			c: newCustomerList,
			want: []*Customer{newCustomerList[1]},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.List(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCustomerList_Search(t *testing.T) {
	type args struct {
		id        int
		firstName string
		lastName  string
		email     string
	}

	newCustomerList := fillCustomer()
	delete(newCustomerList, 2)

	tests := []struct {
		name    string
		c       CustomerList
		args    args
		want    []*Customer
		wantErr bool
	}{
		{
			name: "Testing-Search-1",
			c : newCustomerList,
			args: args{id: 1},
			want: []*Customer{newCustomerList[1]},
			wantErr: false,
		},
		{
			name: "Testing-Search-2",
			c : newCustomerList,
			args: args{id: 123},
			wantErr: true,
		},
		{
			name: "Testing-Search-3",
			c : newCustomerList,
			args: args{email: "hello@world.com"},
			want: []*Customer{newCustomerList[1]},
			wantErr: false,
		},
		{
			name: "Testing-Search-4",
			c : newCustomerList,
			args: args{firstName: "Hello1"},
			want: []*Customer{newCustomerList[1]},
			wantErr: false,
		},
		{
			name: "Testing-Search-5",
			c : newCustomerList,
			args: args{email: "hello.World@gmail.com"},
			want : nil,
			wantErr: true,
		},
		{
			name: "Testing-Search-6",
			c : newCustomerList,
			args: args{firstName:"Himanshu", lastName: "Singh"},
			want : nil,
			wantErr: true,
		},
		{
			name : "Testing-Search-7",
			c : newCustomerList,
			args : args{lastName: "World"},
			want : []*Customer{newCustomerList[1]},
			wantErr: false,
		},
		{
			name : "Testing-Search-8",
			c : newCustomerList,
			args : args{firstName: "Golang", lastName: "Google"},
			want: nil,
			wantErr: true,
		},
		{
			name : "Testing-Search-9",
			c : newCustomerList,
			args : args{firstName: "Hello1", lastName: "World"},
			want : []*Customer{newCustomerList[1]},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.c.Search(tt.args.id, tt.args.firstName, tt.args.lastName, tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCustomerList_Update(t *testing.T) {

	newCustomerList := fillCustomer()

	type args struct {
		id        int
		firstName string
		lastName  string
		email     string
		phoneNo   int
	}

	wantCustomer := newCustomerList[1]
	tests := []struct {
		name    string
		c       CustomerList
		args    args
		want    *Customer
		wantErr bool
	}{
		{
			name: "Testing-Update-1",
			c : newCustomerList,
			args: args{id: 1, firstName: "Hello", lastName: "World", email: "helloWorld@gmail.com", phoneNo: 4254},
			want: wantCustomer,
			wantErr: false,
		},
		{
			name: "Testing-Update-2",
			c : newCustomerList,
			args : args{id: 22312, firstName: "Golang", lastName: "Org", email : "go.org", phoneNo: 452523},
			wantErr: true,
		},
		{
			name: "Testing-Update-3",
			c : newCustomerList,
			args : args{id: 2, firstName: "Golang", lastName: "Org", email : "world@hello.com", phoneNo: 452523},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.c.Update(tt.args.id, tt.args.firstName, tt.args.lastName, tt.args.email, tt.args.phoneNo)
			wantCustomer.FirstName = "Hello"
			wantCustomer.LastName = "World"
			wantCustomer.Email = "helloWorld@gmail.com"
			wantCustomer.PhoneNo = 4254
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
