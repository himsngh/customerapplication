package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func main() {

	iCustomer := NewCustomerList()
	handleFunc(iCustomer)
}

func handleFunc(iCustomer ICustomerList){

	// Create customer using the query string
	http.HandleFunc("/api/v1/user", func(w http.ResponseWriter, r *http.Request) {
		createCustomer(w, r, iCustomer)
	})
	// Using request body to send json object to create customer
	http.HandleFunc("/api/v1/create", func(w http.ResponseWriter, r *http.Request) {
		createCustomers(w, r, iCustomer)
	})

	http.HandleFunc("/api/v1/users", func(w http.ResponseWriter, r *http.Request) {
		listAllCustomer(w, r ,iCustomer)
	})

	http.HandleFunc("/api/v1/users/delete", func(w http.ResponseWriter, r *http.Request) {
		deleteCustomer(w, r, iCustomer)
	})

	http.HandleFunc("/api/v1/users/update", func(w http.ResponseWriter, r *http.Request) {
		updateCustomer(w,r, iCustomer)
	})

	http.HandleFunc("/api/v1/users/search", func(w http.ResponseWriter, r *http.Request) {
		searchCustomer(w, r, iCustomer)
	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

func createCustomer(w http.ResponseWriter, r *http.Request, iCustomer ICustomerList){

	if r.Method != http.MethodPost {
		http.Error(w, "method not allowed ",http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	query:= r.URL.Query()

	id, err := strconv.Atoi(query.Get("id"))
	if err != nil {
		http.Error(w, err.Error(),http.StatusBadRequest)
		return
	}
	// query["firstName"][0] will panic if the firstName field is removed from the url.
	firstName := query.Get("firstName")
	lastName := query.Get("lastName")
	email := query.Get("email")
	phoneNo, _ := strconv.Atoi(query.Get("phoneNo"))

	newCustomer := NewCustomer(id, firstName,lastName,email,phoneNo)
	err = iCustomer.Add(newCustomer)
	if err != nil {
		http.Error(w,err.Error(),http.StatusConflict)
		return
	}

	fmt.Println("New User Created", *newCustomer)
	_ = json.NewEncoder(w).Encode(newCustomer)
}


func createCustomers(w http.ResponseWriter, r *http.Request, iCustomer ICustomerList){

	if r.Method != http.MethodPost {
		http.Error(w, "method not allowed ",http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	var customer Customer
	err := json.NewDecoder(r.Body).Decode(&customer)
	if err != nil {
		http.Error(w, "Error : " + err.Error(), http.StatusInternalServerError)
		return
	}

	err = iCustomer.Add(& customer)
	if err != nil {
		http.Error(w,err.Error(),http.StatusConflict)
		return
	}

	fmt.Println("New User Created", customer)
	_ = json.NewEncoder(w).Encode(customer)
}


// List all the users of the database
func listAllCustomer(w http.ResponseWriter, r *http.Request, iCustomer ICustomerList){

	if r.Method != http.MethodGet {
		http.Error(w, "method not allowed ",http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type","application/json;charset=UTF-8")

	customers := iCustomer.List()
	_ = json.NewEncoder(w).Encode(customers)
}

// Delete the user from the database
func deleteCustomer(w http.ResponseWriter, r *http.Request, iCustomer ICustomerList){

	if r.Method != http.MethodDelete {
		http.Error(w, "method not allowed ",http.StatusMethodNotAllowed)
		return
	}

	query:= r.URL.Query()

	id, err := strconv.Atoi(query.Get("id"))
	if err != nil {
		http.Error(w, "Error parsing the id", http.StatusBadRequest)
		return
	}

	err = iCustomer.Delete(id)
	if err != nil {
		http.Error(w, err.Error(),http.StatusNotFound)
		return
	}

	fmt.Println("User Deleted with user id ", id)
	_,_ = fmt.Fprint(w, "User Deleted with id : ", id)
}

func updateCustomer(w http.ResponseWriter, r *http.Request ,iCustomer ICustomerList) {

	if r.Method != http.MethodPut && r.Method != http.MethodPatch {
		http.Error(w, "method not allowed ",http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	query:= r.URL.Query()

	id, _ := strconv.Atoi(query.Get("id"))
	firstName := query.Get("firstName")
	lastName := query.Get("lastName")
	email := query.Get("email")
	phoneNo, _ := strconv.Atoi(query.Get("phoneNo"))

	// If method is put the user should provide all the attributes to update.
	if r.Method == http.MethodPut && (id == 0 || firstName == "" || lastName == "" || email == "" || phoneNo == 0) {
		http.Error(w, "method put not all arguments provided", http.StatusBadRequest)
		return
	}

	updatedCustomer , err := iCustomer.Update(id,firstName,lastName,email,phoneNo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	fmt.Println("Customer Updated")
	_ = json.NewEncoder(w).Encode(updatedCustomer)
}


func searchCustomer(w http.ResponseWriter, r *http.Request, iCustomer ICustomerList){

	if r.Method != http.MethodGet {
		http.Error(w, "method not allowed",http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	query := r.URL.Query()

	id , _ := strconv.Atoi(query.Get("id"))
	firstName := query.Get("firstName")
	lastName := query.Get("lastName")
	email := query.Get("email")

	searchList, err := iCustomer.Search(id,firstName,lastName,email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	_ = json.NewEncoder(w).Encode(searchList)
}
