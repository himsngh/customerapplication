package main

import "errors"

// Customer Data Structure
type Customer struct {
	ID        int `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	PhoneNo   int	`json:"phoneNo"`
}

func NewCustomer(id int, firstName , lastName, email string, phoneNo int) *Customer {
	return &Customer{
		ID:        id,
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		PhoneNo:   phoneNo,
	}
}

type ICustomerList interface {
	Add(customer *Customer) error
	List() []*Customer
	Update(int, string, string, string, int) (*Customer, error)
	Delete(int) error
	Search(int, string, string, string) ([]*Customer, error)
}

type CustomerList map[int]*Customer


func NewCustomerList() ICustomerList {
	return &CustomerList{}
}

func (c CustomerList) Add(customer *Customer) error {
	if _, ok := c[customer.ID]; ok {
		return errors.New("customer id already exist")
	}

	if customer.ID == 0 {
		return errors.New("customer id cannot be 0")
	}

	if customer.Email == "" {
		return errors.New("email cannot be null")
	}
	for _, value := range c {
		if value.Email == customer.Email {
			return errors.New("email id already taken")
		}
	}

	c[customer.ID] = customer
	return nil
}

func (c CustomerList) List() []*Customer {
	customerList := make([]*Customer,0)

	for _, value := range c {
		customerList = append(customerList, value)
	}
	return customerList
}

func (c CustomerList) Update(id int, firstName , lastName, email string, phoneNo int) (*Customer, error) {

	if _, ok := c[id]; !ok  || id == 0 {
		return nil, errors.New("customer does not exist")
	}

	customer := c[id]

	if email != "" {

		for _, value := range c {
			if value.Email == email {
				return nil, errors.New("email id already taken")
			}
		}
		customer.Email = email
	}

	if firstName != "" {
		customer.FirstName = firstName
	}

	if lastName != "" {
		customer.LastName = lastName
	}

	if phoneNo != 0 {
		customer.PhoneNo = phoneNo
	}
	return customer, nil
}

func (c CustomerList) Delete(id int) error {
	if _, ok := c[id]; !ok {
		return errors.New("customer does not exist")
	}

	delete(c, id)
	return nil
}

func (c CustomerList) Search(id int, firstName, lastName, email string) ([]*Customer, error){

	listOfCustomer := make(map[int]*Customer)

	searchList := make([]*Customer,0)

	if id != 0 {
		if _, ok := c[id]; !ok {
			return nil, errors.New("no match found")
		}
		searchList = append(searchList, c[id])
		return searchList, nil
	}

	if email != "" {
		for _, value := range c {

			if value.Email == email {
				searchList = append(searchList, value)
				return searchList, nil
			}
		}
		return nil, errors.New("no match found")
	}

	for _, value := range c {

		if firstName != "" && lastName != "" && value.FirstName == firstName && value.LastName == lastName {
			listOfCustomer[value.ID] = value

		} else if firstName == "" && lastName == value.LastName {
			listOfCustomer[value.ID] = value

		} else if lastName == "" && firstName == value.FirstName{
			listOfCustomer[value.ID] = value
		}
	}

	if len(listOfCustomer) == 0 {
		return nil, errors.New("no match found")
	}

	for _, value := range listOfCustomer {
		newCustomer := value
		searchList = append(searchList, newCustomer)
	}

	return searchList, nil
}