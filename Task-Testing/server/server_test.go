package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_createCustomer(t *testing.T) {

	req, err := http.NewRequest("POST", "/api/v1/user", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := req.URL.Query()
	q.Add("id", "1")
	q.Add("firstName", "abc")
	q.Add("lastName", "pqr")
	q.Add("email", "abc@pqr.com")
	q.Add("phoneNo", "12345")
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		createCustomer(w, r, NewCustomerList())
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// json encoder return with a \n in the end. (remember)
	expected := `{"id":1,"firstName":"abc","lastName":"pqr","email":"abc@pqr.com","phoneNo":12345}`+ "\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func Test_createCustomers(t *testing.T) {

	var jsonStr = []byte(`{"id":2,"firstName":"xyz","lastName":"pqr","email":"xyz2@pqr.com","phoneNo":12345}`)

	req, err := http.NewRequest("POST", "/api/v1/create", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		createCustomers(w, r, NewCustomerList())
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"id":2,"firstName":"xyz","lastName":"pqr","email":"xyz2@pqr.com","phoneNo":12345}` + "\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func Test_deleteCustomer(t *testing.T) {

	customerList := CustomerList{
		1 : NewCustomer(1,"Hello1","World","hello@world.com",5323),
	}

	req, err := http.NewRequest("DELETE", "/api/v1/users/delete", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := req.URL.Query()
	q.Add("id", "1")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		deleteCustomer(w, r, customerList)
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := "User Deleted with id : 1"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func Test_listAllCustomer(t *testing.T) {

	customerList := CustomerList{
		1 : NewCustomer(1,"Hello1","World","hello@world.com",5323),
	}

	req, err := http.NewRequest("GET", "/api/v1/users/delete", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		listAllCustomer(w, r, customerList)
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `[{"id":1,"firstName":"Hello1","lastName":"World","email":"hello@world.com","phoneNo":5323}]` + "\n"

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func Test_searchCustomer(t *testing.T) {
	customerList := CustomerList{
		1 : NewCustomer(1,"Hello1","World","hello@world.com",5323),
	}

	req, err := http.NewRequest("GET", "/api/v1/users/search", nil)
	if err != nil {
		t.Fatal(err)
	}
	q := req.URL.Query()
	q.Add("id", "1")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		searchCustomer(w, r, customerList)
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `[{"id":1,"firstName":"Hello1","lastName":"World","email":"hello@world.com","phoneNo":5323}]` +"\n"
	if rr.Body.String() != string(expected) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}

}

func Test_updateCustomer(t *testing.T) {
	customerList := CustomerList{
		1 : NewCustomer(1,"Hello1","World","hello@world.com",5323),
	}

	req, err := http.NewRequest("PATCH", "/api/v1/users/search", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := req.URL.Query()
	q.Add("id", "1")
	q.Add("firstName", "Hello")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		updateCustomer(w, r, customerList)
	})

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"id":1,"firstName":"Hello","lastName":"World","email":"hello@world.com","phoneNo":5323}` + "\n"
	if rr.Body.String() != string(expected) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}