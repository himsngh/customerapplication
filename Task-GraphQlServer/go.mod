module gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer

go 1.16

require (
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.10.0
	go.appointy.com/jaal v0.0.1
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
