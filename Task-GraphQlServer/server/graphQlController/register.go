package graphQlController

import (
	"context"
	"log"

	"gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer/server/database"
	"gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer/server/model"
	"go.appointy.com/jaal/jerrors"
	"go.appointy.com/jaal/schemabuilder"
)


type GraphQlServer struct {
	Db *database.Database
}

func RegisterObject(schema *schemabuilder.Schema) {

	payload := schema.Object("Customer", model.Customer{})
	payload.FieldFunc("id", func(ctx context.Context, in *model.Customer) schemabuilder.ID {
		return schemabuilder.ID{Value: in.Id}
	})
	payload.FieldFunc("firstName", func(ctx context.Context, in *model.Customer) string {
		return in.FirstName
	})
	payload.FieldFunc("lastName", func(ctx context.Context, in *model.Customer) string {
		return in.LastName
	})
	payload.FieldFunc("email", func(ctx context.Context, in *model.Customer) string {
		return in.Email
	})
	payload.FieldFunc("phoneNo", func(ctx context.Context, in *model.Customer) int64 {
		return in.PhoneNo
	})


	payload = schema.Object("CreateCustomerPayload", model.CreateCustomerPayload{})
	payload.FieldFunc("payload", func(ctx context.Context, in *model.CreateCustomerPayload) *model.Customer {
		return in.Payload
	})

	payload = schema.Object("UpdateCustomerPayload", model.UpdateCustomerPayload{})
	payload.FieldFunc("payload", func(ctx context.Context, in *model.UpdateCustomerPayload) *model.Customer {
		return in.Payload
	})

	payload = schema.Object("DeleteCustomerPayload", model.DeleteCustomerPayload{})
	payload.FieldFunc("success", func(ctx context.Context, in *model.DeleteCustomerPayload) bool {
		return in.Success
	})

}

func RegisterInput(schema *schemabuilder.Schema) {

	input := schema.InputObject("CustomerInput", model.CustomerInput{})
	input.FieldFunc("firstName", func(target *model.CustomerInput, source *string) {
		target.FirstName = *source
	})
	input.FieldFunc("lastName", func(target *model.CustomerInput, source *string) {
		target.LastName = *source
	})
	input.FieldFunc("email", func(target *model.CustomerInput, source *string) {
		target.Email = *source
	})
	input.FieldFunc("phoneNo", func(target *model.CustomerInput, source *int64) {
		target.PhoneNo = *source
	})


	input = schema.InputObject("CreateCustomerInput", model.CreateCustomerInput{})
	input.FieldFunc("customer", func(target *model.CreateCustomerInput, source *model.CustomerInput) {
		target.Customer = source
	})

	input = schema.InputObject("UpdateCustomerInput", model.UpdateCustomerInput{})
	input.FieldFunc("customer", func(target *model.UpdateCustomerInput, source *model.CustomerInput) {
		target.Customer = source
	})
	input.FieldFunc("id", func(target *model.UpdateCustomerInput, s2 *string) {
		target.Id= *s2
	})


	input = schema.InputObject("DeleteCustomerInput", model.DeleteCustomerInput{})
	input.FieldFunc("id", func(target *model.DeleteCustomerInput, source *string) {
		target.Id = *source
	})

}

func (gs *GraphQlServer) RegisterQuery(schema *schemabuilder.Schema) {

	schema.Query().FieldFunc("customer", func(ctx context.Context, args struct{
		Id *schemabuilder.ID
	}) *model.Customer {

		customer, err := gs.Db.GetCustomer(args.Id.Value)
		if err != nil {
			return nil
		}

		return customer
	})

	schema.Query().FieldFunc("customers", func(ctx context.Context, args struct {}) []*model.Customer {

		customerList := gs.Db.ListCustomer()
		return customerList
	})
}

func (gs *GraphQlServer) RegisterMutation(schema *schemabuilder.Schema) {

	schema.Mutation().FieldFunc("createCustomer", func(ctx context.Context, args struct {
		Input *model.CreateCustomerInput
	}) (*model.CreateCustomerPayload, error) {

			newCustomer , err := gs.Db.CreateCustomer(args.Input.Customer)
			if err != nil {
				log.Println("Error while creating.", err)
				return nil, jerrors.ConvertError(err)
			}

			log.Println("New Customer Created")
			return &model.CreateCustomerPayload{Payload: newCustomer}, nil
	})


	schema.Mutation().FieldFunc("updateCustomer", func(ctx context.Context, args struct {
		Input *model.UpdateCustomerInput
	}) (*model.UpdateCustomerPayload, error) {

		if args.Input.Id == "" {
			return nil, &jerrors.Error{Message: "id cannot be null"}
		}

		updatedCustomer, err := gs.Db.UpdateCustomer(args.Input.Customer, args.Input.Id)
		if err != nil {
			log.Println("Error Updating : ", err)
			return nil, jerrors.ConvertError(err)
		}

		log.Println("Customer Updated id ")
		return &model.UpdateCustomerPayload{Payload: updatedCustomer}, nil
	})


	schema.Mutation().FieldFunc("deleteCustomer", func(ctx context.Context, args struct {
		Input *model.DeleteCustomerInput
	}) (*model.DeleteCustomerPayload, error) {

		err := gs.Db.DeleteCustomer(args.Input.Id)
		if err != nil {
			return nil, jerrors.ConvertError(err)
		}

		log.Println("Customer Deleted")
		return &model.DeleteCustomerPayload{Success: true}, nil
	})
}
