package model

type Customer struct {
	Id  string
	FirstName  string
	LastName  string
	Email  string
	PhoneNo int64
}

type CustomerInput struct {
	FirstName  string
	LastName  string
	Email  string
	PhoneNo int64
}

type CreateCustomerInput struct {
	 Customer *CustomerInput
}

type CreateCustomerPayload struct {
	Payload *Customer
}

type UpdateCustomerInput struct {
	Id string
	Customer *CustomerInput
}

type UpdateCustomerPayload struct {
	Payload *Customer
}

type DeleteCustomerInput struct {
	Id string
}

type DeleteCustomerPayload struct {
	Success bool
}
