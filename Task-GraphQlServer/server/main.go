package main

import (

	"log"
	"net/http"

	"gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer/server/database"
	"gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer/server/graphQlController"
	"go.appointy.com/jaal"
	"go.appointy.com/jaal/introspection"
	"go.appointy.com/jaal/schemabuilder"
)

func main() {

	sb := schemabuilder.NewSchema()

	graphQlController.RegisterObject(sb)
	graphQlController.RegisterInput(sb)

	s := &graphQlController.GraphQlServer{
		Db: database.NewDatabase(),
	}

	s.RegisterQuery(sb)
	s.RegisterMutation(sb)

	schema, err := sb.Build()
	if err != nil {
		log.Fatal(err)
	}

	introspection.AddIntrospectionToSchema(schema)

	http.Handle("/api/v1/graphql", jaal.HTTPHandler(schema))
	log.Println("Running....")
	err = http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatal(err)
	}

}
