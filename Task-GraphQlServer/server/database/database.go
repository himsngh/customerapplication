package database

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/himsngh/CustomerApplication/Task-GraphQlServer/server/model"
	"log"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

type Database struct {
	db *sql.DB
}

func NewDatabase() *Database {
	return &Database{
		db:Connect(),
	}
}

func Connect() *sql.DB{

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"localhost", 5432, "himanshuappointy", "password", "himanshuappointy")

	psqlDb, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalln("database connection failed ", err)
	}

	err = psqlDb.Ping()
	if err != nil {
		log.Println("ping database failed")
		panic(err)
	}

	log.Println("database connected..........................")
	createTableIfNotExist(psqlDb)

	return psqlDb
}

func createTableIfNotExist(psql *sql.DB) {
	_, err := psql.Exec("create table if not exists customer_app (" +
		"id varchar(100) primary key ," +
		"firstname varchar(100)," +
		"lastname varchar(100) , " +
		"email varchar(100) not null, " +
		"phone_no bigint);")

	if err != nil {
		log.Println("error creating table.")
	}
}


func (d *Database) CreateCustomer(newCustomer *model.CustomerInput) (*model.Customer, error) {

	id := uuid.Must(uuid.NewRandom()).String()
	_, err := d.db.Exec("INSERT INTO customer_app VALUES ($1, $2, $3, $4, $5)",
		id, newCustomer.FirstName, newCustomer.LastName, newCustomer.Email, newCustomer.PhoneNo)

	if err != nil {
		return nil, errors.New("Failed to insert the user : " + err.Error())
	}

	return &model.Customer{
		Id:        id,
		FirstName: newCustomer.FirstName,
		LastName:  newCustomer.LastName,
		Email:     newCustomer.Email,
		PhoneNo:   newCustomer.PhoneNo,
	}, nil
}

func (d *Database) GetCustomer(id string) (*model.Customer, error) {

	row := d.db.QueryRow("select * from customer_app where id = $1", id)

	customer := &model.Customer{}

	err := row.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Email, &customer.PhoneNo)
	if err != nil {
		return nil, errors.New("No such customer exist : " + err.Error())
	}

	return customer, nil
}


func (d *Database) ListCustomer() []*model.Customer {

	rows, _ := d.db.Query("select * from customer_app")

	customers := queryCustomers(rows)
	return customers
}

func (d *Database) UpdateCustomer(customer *model.CustomerInput, customerId string) (*model.Customer,error) {

	res, err := d.db.Exec("update customer_app set firstname=$2, lastname=$3, email=$4, phone_no=$5 where id=$1",
		customerId, customer.FirstName, customer.LastName, customer.Email, customer.PhoneNo)
	if  err != nil {
		return nil, errors.New("error updating the row : " + err.Error())
	}
	if n, _ := res.RowsAffected(); n == 0 {
		return nil, errors.New("error updating, no such customer exist")
	}

	return &model.Customer{
		Id:       customerId,
		FirstName: customer.FirstName,
		LastName:  customer.LastName,
		Email:     customer.Email,
		PhoneNo:   customer.PhoneNo,
	},nil
}

func (d *Database) DeleteCustomer(id string) error {

	res, err := d.db.Exec("delete from customer_app where id = $1", id)
	if err != nil {
		return errors.New("error deleting the customer : " + err.Error())
	}

	if n, _ := res.RowsAffected(); n == 0 {
		return errors.New("no such customer exist")
	}
	return nil
}

func queryCustomers(rows *sql.Rows) []*model.Customer {

	customers := make([]*model.Customer, 0)

	for rows.Next() {

		customer := &model.Customer{}
		err := rows.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Email, &customer.PhoneNo)
		if err != nil {
			log.Println(err.Error())
			continue
		}

		customers = append(customers, customer)
	}
	return customers
}