package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type httpPostBody struct {
	Query     string                 `json:"query"`
	Variables map[string]interface{} `json:"variables"`
}


func main() {
		createCustomer()
		listCustomer()
		getCustomerById()
		deleteCustomer()
		updateCustomer()
}

func createCustomer(){

	jsonData := `mutation { 
                	createCustomer (
						input :  {
						customer : {
							firstName : "Himanshu"
							lastName : "Ranjan"
							email : "himanshuranjan@appointy.com"
							phoneNo : 89353939
						}
                		}) {
						payload {
							id
							firstName
						}
					}
            	}`
	reqData := httpPostBody{Query: jsonData}
    jsonValue, err := json.Marshal(reqData)
    if err != nil {
    	log.Println("Error marshaling json : ", err)
    	return
	}

	request, err := http.NewRequest("POST","http://localhost:8080/api/v1/graphql",  bytes.NewBuffer(jsonValue))
	client := &http.Client{}
	res, err := client.Do(request)
	if err != nil {
		log.Println("Error posting json : ", err)
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("error reading response body ", err)
	}

	log.Println(string(data))
}

func listCustomer(){
	jsonData := `query { 
                	customers {
							firstName
							lastName
							email 
							phoneNo
						}
					}`

	reqData := httpPostBody{Query: jsonData}
	jsonValue, err := json.Marshal(reqData)
	if err != nil {
		log.Println("Error marshaling json : ", err)
		return
	}

	request, err := http.NewRequest("POST","http://localhost:8080/api/v1/graphql",  bytes.NewBuffer(jsonValue))
	client := &http.Client{}
	res, err := client.Do(request)

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println(string(data))
}

func getCustomerById() {
	jsonData := `query { 
                	customer(
						id : "7be657a4-e933-40c9-8f2c-52dc3c3b005f"
							) {
							firstName
							lastName
							email 
							phoneNo
						}
					}`

	reqData := httpPostBody{Query: jsonData}
	jsonValue, err := json.Marshal(reqData)
	if err != nil {
		log.Println("Error marshaling json : ", err)
		return
	}

	request, err := http.NewRequest("POST","http://localhost:8080/api/v1/graphql",  bytes.NewBuffer(jsonValue))
	client := &http.Client{}
	res, err := client.Do(request)

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println(string(data))
}
func deleteCustomer(){
	jsonData := `mutation { 
                	deleteCustomer(
						input : {
									id : "7be657a4-e933-40c9-8f2c-52dc3c3b005f"
							}) {
							success
						}
					}`

	reqData := httpPostBody{Query: jsonData}
	jsonValue, err := json.Marshal(reqData)
	if err != nil {
		log.Println("Error marshaling json : ", err)
		return
	}

	request, err := http.NewRequest("POST","http://localhost:8080/api/v1/graphql",  bytes.NewBuffer(jsonValue))
	client := &http.Client{}
	res, err := client.Do(request)

	data , _ := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()

	fmt.Println(string(data))
}

func updateCustomer(){

	jsonData := `mutation { 
                	updateCustomer (
						input :  {
						id : "7ba28bba-d57e-42cf-8708-363c6e6cfa4f"
						customer : {
							firstName : "Himanshu"
							lastName : "Ranjan"
							email : "helloran@appointy.com"
							phoneNo : 89353939
						}
                		}) {
						payload {
							id
							firstName
						}
					}
            	}`
	reqData := httpPostBody{Query: jsonData}
	jsonValue, err := json.Marshal(reqData)
	if err != nil {
		log.Println("Error marshaling json : ", err)
		return
	}

	request, err := http.NewRequest("POST","http://localhost:8080/api/v1/graphql",  bytes.NewBuffer(jsonValue))
	client := &http.Client{}
	res, err := client.Do(request)
	if err != nil {
		log.Println("Error posting json : ", err)
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("error reading response body ", err)
	}

	log.Println(string(data))
}
