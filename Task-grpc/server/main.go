package main

import (
	"context"
	"log"
	"net/http"
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	pb "gitlab.com/himsngh/CustomerApplication/Task-grpc/generate/customerProto"
	"gitlab.com/himsngh/CustomerApplication/Task-grpc/server/controller"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
)

func main() {
	// ------------------------------- Serving both gRPC and HTTP1.1 on different ports -----------------------------//

	// ------------------------------- Serving the gRPC server ------------------------------------------------------ //
	//
	//lis, err := net.Listen("tcp", "localhost:8080")
	//if err != nil {
	//	log.Fatalln("Failed to listen : ", err)
	//}
	//
	//grpcServer := grpc.NewServer(grpc.Creds(local.NewCredentials()))
	//cs := controller.NewCustomerServiceController()
	//pb.RegisterCustomersServer(grpcServer, cs)
	//
	//go func() {
	//	log.Fatalln(grpcServer.Serve(lis))
	//}()
	//
	//// ------------------------------------------ Serving the rest server ----------------------------------------- //
	//
	//grpcHttpMux := runtime.NewServeMux()
	//dp := []grpc.DialOption{grpc.WithInsecure()}
	//
	//err = pb.RegisterCustomersHandlerFromEndpoint(context.Background(), grpcHttpMux, "localhost:8080",dp)
	////err := pb.RegisterCustomersHandlerServer(context.Background(), grpcHttpMux, cs)
	//if err != nil {
	//	log.Fatalln("Failed to register gateway:", err)
	//}
	//
	//gwServer := &http.Server{
	//	Addr:    ":8090",
	//	Handler: grpcHttpMux,
	//}
	//
	//log.Fatalln(gwServer.ListenAndServe())
	//}

	//--------------------------- Serving both Http2(grpc) and HTTP1 (rest) on the same port. :) -------------------- //

	//	 -------------------------------------------- Using cmux package -------------------------------------------- //
	//	lis, err := net.Listen("tcp", "localhost:8080")
	//	if err != nil {
	//		log.Fatalln("error listening ", err)
	//	}
	//
	//	m := cmux.New(lis)
	//
	//	//grpcl := m.Match(cmux.HTTP2HeaderField("content-type", "application/grpc")) ?? not working
	//	grpcL := m.Match(cmux.HTTP2())
	//	httpL := m.Match(cmux.HTTP1Fast())
	//
	//	grpcServer := grpc.NewServer()
	//	cs := controller.NewCustomerServiceController()
	//	pb.RegisterCustomersServer(grpcServer, cs)
	//
	//	httpServer := runtime.NewServeMux()
	//	httpS := &http.Server{
	//		Handler: httpServer,
	//	}
	//
	//	err = pb.RegisterCustomersHandlerServer(context.Background(), httpServer, cs)
	//
	//	go grpcServer.Serve(grpcL)
	//	go httpS.Serve(httpL)
	//
	//	log.Fatal(m.Serve())
	//}

	grpcServer := grpc.NewServer()
	cs := controller.NewCustomerServiceController()
	pb.RegisterCustomersServer(grpcServer, cs)

	httpS := runtime.NewServeMux()
	err := pb.RegisterCustomersHandlerServer(context.Background(), httpS, cs)
	if err != nil {
		log.Fatal(err)
	}

	handler := handler(grpcServer, httpS)

	log.Fatal(http.ListenAndServe(":8080", handler))
}

func handler(grpcS *grpc.Server, httpS http.Handler) http.Handler {
	return h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcS.ServeHTTP(w, r)
		} else {
			httpS.ServeHTTP(w, r)
		}
	}), &http2.Server{})
}

