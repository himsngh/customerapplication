package controller

import (
	"context"
	"errors"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
	"sync"

	pb "gitlab.com/himsngh/CustomerApplication/Task-grpc/generate/customerProto"
)

type CustomerServiceController struct {
	pb.UnimplementedCustomersServer
	mu sync.Mutex
	db map[int32]*pb.Customer
}

func NewCustomerServiceController() pb.CustomersServer {

	return &CustomerServiceController{
		db:   make(map[int32]*pb.Customer),
	}
}

func(s *CustomerServiceController) CreateCustomer(ctx context.Context, r *pb.CreateCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetCustomer() == nil {
		return nil, errors.New("no customer provided")
	}

	if _, ok := s.db[r.Customer.Id]; ok {
		return nil, errors.New("customer already exist")
	}

	if r.Customer.GetEmail() == "" {
		return nil, errors.New("email cannot be null")
	}

	for _, customer := range s.db {
		if customer.Email == r.Customer.Email {
			return nil, errors.New("customer email already exist")
		}
	}

	s.db[r.Customer.Id] = r.Customer
	log.Println("New Customer Created with id : ", r.Customer.Id)

	return r.Customer, nil
}


func (s *CustomerServiceController) GetCustomer(ctx context.Context, r *pb.GetCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if _, ok := s.db[r.Id]; !ok {
		return nil, errors.New("customer does not exist")
	}

	return s.db[r.Id], nil
}

func (s *CustomerServiceController) ListCustomers(context.Context, *emptypb.Empty) (*pb.ListCustomersResponse, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	customers := make([]*pb.Customer, 0)

	for _, customer := range s.db {
		customers = append(customers, customer)
	}

	return &pb.ListCustomersResponse{Customers: customers}, nil
}

func (s *CustomerServiceController) UpdateCustomer(ctx context.Context, r *pb.UpdateCustomerRequest) (*pb.Customer, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetCustomer() == nil {
		return nil, errors.New("no customer provided")
	}

	if _, ok := s.db[r.Customer.Id]; !ok {
		return nil, errors.New("customer does not exist")
	}

	oldCustomer := s.db[r.Customer.Id]

	if oldCustomer.Email != r.GetCustomer().GetEmail() {
		for _, customer := range s.db {
			if customer.Email == r.Customer.Email {
				return nil, errors.New("cannot update customer. update email already exist with different user")
			}
		}
	}

	s.db[r.Customer.Id] = r.Customer
	log.Println("customer updated with id : ", r.Customer.Id)

	return r.Customer, nil
}

func (s *CustomerServiceController) DeleteCustomer(ctx context.Context, r *pb.DeleteCustomerRequest) (*emptypb.Empty, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if _, ok := s.db[r.Id]; !ok {
		return nil, errors.New("customer does not exist")
	}

	delete(s.db, r.Id)
	log.Println("customer deleted with id : ", r.Id)

	return &emptypb.Empty{}, nil
}

func (s *CustomerServiceController) SearchCustomer(ctx context.Context, r *pb.SearchCustomerRequest) (*pb.SearchCustomerResponse, error) {

	s.mu.Lock()
	defer s.mu.Unlock()

	if r.GetFirstName() == "" && r.GetLastName() == "" && r.GetEmail() == "" && r.GetPhoneNo() == 0 {
		return nil, errors.New("no search parameter provided")
	}

	customers := make([]*pb.Customer, 0)

	// If the user has provided the email in the query then there may exist only one user as the email is unique.
	if r.GetEmail() != "" {
		newCustomer := &pb.Customer{}

		for _, customer := range s.db {
			if customer.Email == r.GetEmail() {
				newCustomer = customer
			}
		}

		if newCustomer.Email != r.GetEmail() {
			return nil, customerDoesntExistError()
		}

		if r.GetFirstName() != "" && r.GetFirstName() != newCustomer.FirstName{
			return nil, customerDoesntExistError()
		}

		if r.GetLastName() != "" && r.GetLastName() != newCustomer.LastName {
			return nil, customerDoesntExistError()
		}

		if r.GetPhoneNo() != 0 &&  r.GetPhoneNo() != newCustomer.PhoneNo {
			return nil, customerDoesntExistError()
		}

		customers = append(customers, newCustomer)
		return &pb.SearchCustomerResponse{Customers: customers}, nil
	}

	// Searching the customer in the database with customer details as firstName, lastName and phoneNo.
	for _, customer := range s.db {
		newCustomer := customer

		if r.GetFirstName() != "" && r.GetLastName() != "" && r.GetPhoneNo() != 0 {
			if r.GetFirstName() != newCustomer.FirstName || r.GetLastName() != newCustomer.LastName || r.GetPhoneNo() != newCustomer.PhoneNo {
				continue
			}

		} else if r.GetFirstName() == "" {

			if r.GetLastName() != "" && r.GetPhoneNo() != 0 && (r.GetLastName() != newCustomer.LastName || r.GetPhoneNo() != newCustomer.PhoneNo) {
				continue

			} else if r.GetLastName() == "" && r.GetPhoneNo() != newCustomer.PhoneNo {
				continue

			} else if r.GetPhoneNo() == 0 && r.GetLastName() != newCustomer.LastName {
				continue
			}

		} else if r.GetFirstName() != newCustomer.FirstName {
			continue

		} else if r.GetLastName() != "" && r.GetLastName() != newCustomer.LastName {
			continue

		} else if r.GetPhoneNo() != 0 && r.GetPhoneNo() != newCustomer.PhoneNo {
			continue
		}

		customers = append(customers, newCustomer)
	}

	if len(customers) == 0 {
		return nil, customerDoesntExistError()
	}

	return &pb.SearchCustomerResponse{Customers: customers}, nil
}

func customerDoesntExistError() error{
	return  errors.New("no such customer exist")
}
